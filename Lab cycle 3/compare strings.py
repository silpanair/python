#Write a function called compare which takes two strings S1 and S2 and an
#integer n as arguments. The function should return True if the first n characters
#of both the strings are the same else the function should return False.

def compare(s1,s2,n):
    if(s1[0:n]==s2[0:n]):
        print("True")
    else:
        print("False")
s1=input("Enter the first string\n")
s2=input("Enter the second string\n")
a=len(s1)
b=len(s2)
n=int(input("Enter the number less than or equal to the minimum of %d and %d\n" %(a,b)))
compare(s1,s2,n)