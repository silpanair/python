# program to print the Fibonacci series using recursion.

def fib(n):
	if(n<=1):
		return n
	else:
		return(fib(n-1)+fib(n-2))
n=int(input("Enter a positive number upto which the fibonacci series is to be obtained\n"))
print("The Fibonacci series with %d terms is \n" %n)
for i in range(0,n):
	print(fib(i), end=" ")
print("\n")