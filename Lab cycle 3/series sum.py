#program to sum the series 1/1! + 4/2! + 27/3! + ..... + nth term. [ Hint :
#Use function to find the factorial of a number].

def fact(i):
	if(i==0):
		return 1
	else:
		return(i*fact(i-1))
n=int(input("Enter number of terms\n"))
sum=0
for i in range(1,n+1):
	sum=sum+((i**i)/fact(i))
print("The sum of the series is ",sum)