#program to add variable length integer arguments passed to the function.
#[Also demo the use of docstrings]

def sum1(*args):
    """variable length integer arguments"""
    sum=0
    for i in args:
        sum=sum+int(i)
    return(sum)
print(sum1.__doc__)
print(sum1(1,2,3,4,5,6))